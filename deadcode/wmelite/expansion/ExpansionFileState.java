package org.deadcode.wmelite.expansion;

public interface ExpansionFileState {

	public boolean mountRequest();
	
	public boolean unmountRequest();
	
	public void obbStateCallback(String path, int state);
	
}
