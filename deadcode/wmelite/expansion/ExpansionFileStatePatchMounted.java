package org.deadcode.wmelite.expansion;

import android.os.storage.OnObbStateChangeListener;
import android.os.storage.StorageManager;
import android.util.Log;

public class ExpansionFileStatePatchMounted implements ExpansionFileState {
	
	private static final String TAG = "StatePatchMounted";
	
	private final ExpansionFileStateMachine stateMachine;
	
	private final StorageManager stMgr;
	
	private final String patchObbFile;
	
	private final OnObbStateChangeListener listener;
	
	public ExpansionFileStatePatchMounted(ExpansionFileStateMachine stateMachine, StorageManager stMgr, String patchObbFile, OnObbStateChangeListener listener)
	{
		this.stateMachine = stateMachine;
		this.stMgr = stMgr;
		this.patchObbFile = patchObbFile;
		this.listener = listener;
	}

	@Override
	public boolean mountRequest() {
        // everything is already mounted
		Log.e(TAG, "Mount request received?");

		return true;
	}

	@Override
	public boolean unmountRequest() {
		Log.i(TAG, "Trying to unmount patch OBB file: " + patchObbFile);

		stateMachine.ist = stateMachine.unmountingPatch;
		
		return stMgr.unmountObb(patchObbFile, false, listener);
	}

	public String getObbMountPath()
	{
		return stMgr.getMountedObbPath(patchObbFile);
	}

	@Override
	public void obbStateCallback(String path, int state) {
		Log.w(TAG, "Unexpected mount result, state=" + state);
	}

}
