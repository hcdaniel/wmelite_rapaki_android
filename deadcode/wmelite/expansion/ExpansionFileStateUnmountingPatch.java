package org.deadcode.wmelite.expansion;

import android.os.storage.OnObbStateChangeListener;
import android.util.Log;

public class ExpansionFileStateUnmountingPatch implements ExpansionFileState {
	
	private static final String TAG = "StateUnmountingPatch";
	
	private final ExpansionFileStateMachine stateMachine;
	
	public ExpansionFileStateUnmountingPatch(ExpansionFileStateMachine stateMachine)
	{
		this.stateMachine = stateMachine;
	}

	@Override
	public boolean mountRequest() {
		// ignore requests in this state
		Log.w(TAG, "Mount request received.");

		return true;
	}

	@Override
	public boolean unmountRequest() {
		// ignore requests in this state
		Log.w(TAG, "Unmount request received.");

		return true;
	}

	@Override
	public void obbStateCallback(String path, int state) {
		if ((state == OnObbStateChangeListener.ERROR_NOT_MOUNTED) || (state == OnObbStateChangeListener.UNMOUNTED))
		{
			Log.i(TAG, "Unmount success, transitioning to StateMainMounted, state=" + state);
			
			stateMachine.ist = stateMachine.mainMounted;
		}
		else
		{
			Log.e(TAG, "Unexpected unmount result, state=" + state);
			
			// try again
			stateMachine.ist = stateMachine.patchMounted;
		}
	}

}
