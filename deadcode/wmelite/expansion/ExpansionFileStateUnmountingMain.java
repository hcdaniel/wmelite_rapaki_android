package org.deadcode.wmelite.expansion;

import android.os.storage.OnObbStateChangeListener;
import android.util.Log;

public class ExpansionFileStateUnmountingMain implements ExpansionFileState {
	
	private static final String TAG = "StateUnmountingMain";
	
	private final ExpansionFileStateMachine stateMachine;
	
	public ExpansionFileStateUnmountingMain(ExpansionFileStateMachine stateMachine)
	{
		this.stateMachine = stateMachine;
	}

	@Override
	public boolean mountRequest() {
		// ignore requests in this state
		Log.w(TAG, "Mount request received.");

		return true;
	}

	@Override
	public boolean unmountRequest() {
		// ignore requests in this state
		Log.w(TAG, "Unmount request received.");

		return true;
	}
	
	@Override
	public void obbStateCallback(String path, int state) {
		if ((state == OnObbStateChangeListener.ERROR_NOT_MOUNTED) || (state == OnObbStateChangeListener.UNMOUNTED))
		{
			Log.i(TAG, "Unmount success, transitioning to StateNoneMounted, state=" + state);

			stateMachine.ist = stateMachine.noneMounted;
		}
		else
		{
			Log.e(TAG, "Unexpected unmount result, state=" + state);
			
			// try again
			stateMachine.ist = stateMachine.mainMounted;
		}
	}

}
