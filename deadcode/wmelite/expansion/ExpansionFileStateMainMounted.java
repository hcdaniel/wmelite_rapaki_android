package org.deadcode.wmelite.expansion;

import android.os.storage.OnObbStateChangeListener;
import android.os.storage.StorageManager;
import android.util.Log;

public class ExpansionFileStateMainMounted implements ExpansionFileState {
	
	private static final String TAG = "StateMainMounted";
	
	private final ExpansionFileStateMachine stateMachine;
	
	private final StorageManager stMgr;
	
	private final String mainObbFile;
	
	private final String patchObbFile;
	
	private final OnObbStateChangeListener listener;
	
	public ExpansionFileStateMainMounted(ExpansionFileStateMachine stateMachine, StorageManager stMgr, String mainObbFile, String patchObbFile, OnObbStateChangeListener listener)
	{
		this.stateMachine = stateMachine;
		this.stMgr = stMgr;
		this.mainObbFile = mainObbFile;
		this.patchObbFile = patchObbFile;
		this.listener = listener;
	}

	@Override
	public boolean mountRequest() {
		Log.i(TAG, "Trying to mount patch OBB file: " + patchObbFile);

		stateMachine.ist = stateMachine.mountingPatch;
		
		return stMgr.mountObb(patchObbFile, null, listener);
	}

	@Override
	public boolean unmountRequest() {
		Log.i(TAG, "Trying to unmount main OBB file: " + mainObbFile);

		stateMachine.ist = stateMachine.unmountingMain;

		return stMgr.unmountObb(mainObbFile, false, listener);
	}
	
	public String getObbMountPath()
	{
		return stMgr.getMountedObbPath(mainObbFile);
	}

	@Override
	public void obbStateCallback(String path, int state) {
		Log.w(TAG, "Unexpected mount result, state=" + state);
	}

}
