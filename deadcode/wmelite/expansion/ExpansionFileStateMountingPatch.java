package org.deadcode.wmelite.expansion;

import android.os.storage.OnObbStateChangeListener;
import android.util.Log;

public class ExpansionFileStateMountingPatch implements ExpansionFileState {
	
	private static final String TAG = "StateMountingPatch";
	
	private final ExpansionFileStateMachine stateMachine;
	
	public ExpansionFileStateMountingPatch(ExpansionFileStateMachine stateMachine)
	{
		this.stateMachine = stateMachine;
	}

	@Override
	public boolean mountRequest() {
		// ignore requests in this state
		Log.w(TAG, "Mount request received.");

		return true;
	}

	@Override
	public boolean unmountRequest() {
		// ignore requests in this state
		Log.w(TAG, "Unmount request received.");
		
		return true;
	}
	
	@Override
	public void obbStateCallback(String path, int state) {
		if ((state == OnObbStateChangeListener.ERROR_ALREADY_MOUNTED) || (state == OnObbStateChangeListener.MOUNTED))
		{
			Log.i(TAG, "Mount success, transitioning to StatePatchMounted, state=" + state);
			
			stateMachine.ist = stateMachine.patchMounted;
		}
		else
		{
			Log.e(TAG, "Unexpected mount result, state=" + state);
			
			// try again
			stateMachine.ist = stateMachine.mainMounted;
		}
	}

}
