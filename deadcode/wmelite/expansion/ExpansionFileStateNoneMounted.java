package org.deadcode.wmelite.expansion;

import android.os.storage.OnObbStateChangeListener;
import android.os.storage.StorageManager;
import android.util.Log;

public class ExpansionFileStateNoneMounted implements ExpansionFileState {

	private static final String TAG = "StateNoneMounted";
	
	private final ExpansionFileStateMachine stateMachine;
	
	private final StorageManager stMgr;
	
	private final String mainObbFile;
	
	private final OnObbStateChangeListener listener;
	
	public ExpansionFileStateNoneMounted(ExpansionFileStateMachine stateMachine, StorageManager stMgr, String mainObbFile, OnObbStateChangeListener listener)
	{
		this.stateMachine = stateMachine;
		this.stMgr = stMgr;
		this.mainObbFile = mainObbFile;
		this.listener = listener;
	}

	@Override
	public boolean mountRequest() {
		Log.i(TAG, "Trying to mount main OBB file: " + mainObbFile);
		
		stateMachine.ist = stateMachine.mountingMain;

		return stMgr.mountObb(mainObbFile, null, listener);
	}

	@Override
	public boolean unmountRequest() {
		// do nothing, everything is already unmounted
		Log.e(TAG, "Unmount request received?");
		
		return true;
	}

	@Override
	public void obbStateCallback(String path, int state) {
		Log.w(TAG, "Unexpected mount result, state=" + state);
	}
}
