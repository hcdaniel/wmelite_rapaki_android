package org.deadcode.wmelite.expansion;

import android.os.Handler;
import android.os.Message;
import android.os.storage.OnObbStateChangeListener;
import android.os.storage.StorageManager;
import android.util.Log;

public class ExpansionFileStateMachine {

	private static final String TAG = "ExpansionFileStateMachine";
	
	protected final ExpansionFileState noneMounted;
	
	protected final ExpansionFileState mountingMain;
	
	protected final ExpansionFileState unmountingMain;
	
	protected final ExpansionFileState mainMounted;
	
	protected final ExpansionFileState mountingPatch;
	
	protected final ExpansionFileState unmountingPatch;
	
	protected final ExpansionFileState patchMounted;
	
	private Handler mainThreadRunHandler;
	
	protected ExpansionFileState soll;
	
	protected ExpansionFileState ist;
	
	public ExpansionFileStateMachine(StorageManager stMgr, String mainObbFile, String patchObbFile)
	{
		ExpansionFileStateChangeListener listener = new ExpansionFileStateChangeListener();
		
		noneMounted = new ExpansionFileStateNoneMounted(this, stMgr, mainObbFile, listener);
		mountingMain = new ExpansionFileStateMountingMain(this);
		unmountingMain = new ExpansionFileStateUnmountingMain(this);
		mainMounted = new ExpansionFileStateMainMounted(this, stMgr, mainObbFile, patchObbFile, listener);
		if ((patchObbFile != null) && (patchObbFile.length() > 0))
		{
			Log.i(TAG, "Found valid OBB patch file name.");
			
			mountingPatch = new ExpansionFileStateMountingPatch(this);
			patchMounted = new ExpansionFileStatePatchMounted(this, stMgr, patchObbFile, listener);
			unmountingPatch = new ExpansionFileStateUnmountingPatch(this);
		}
		else
		{
			Log.i(TAG, "No OBB patch file supplied.");
			
			mountingPatch = null;
			patchMounted = null;
			unmountingPatch = null;
		}
			
		soll = noneMounted;
		ist  = noneMounted;		
	}
	
	public void setCallbackHandler(Handler mainThreadRunHandler)
	{
		this.mainThreadRunHandler = mainThreadRunHandler;
	}
	
	public boolean mountRequest()
	{
		if (patchMounted != null)
		{
			soll = patchMounted;
		}
		else
		{
			soll = mainMounted;
		}
		
		stateMachineTick.sendEmptyMessage(0);
		
		return true;
	}
	
	public boolean unmountRequest()
	{
		soll = noneMounted;
		
		stateMachineTick.sendEmptyMessage(0);
		
		return true;
	}
	
    public String getObbMountPath()
    {
    	return ((ExpansionFileStateMainMounted) mainMounted).getObbMountPath();
    }

    public String getObbMountPathPatch()
    {
    	if (patchMounted != null)
    	{
    		return ((ExpansionFileStatePatchMounted) patchMounted).getObbMountPath();
    	}
    	else
    	{
    		return null;
    	}
    }
	

	/**
     * Listen to events of any mount/unmount.
     */
    public class ExpansionFileStateChangeListener extends OnObbStateChangeListener
    {
        public void onObbStateChange(String path, int state)
        {
        	ist.obbStateCallback(path, state);
        	stateMachineTick.sendEmptyMessageDelayed(0, 10);
        }
    }
    
	private Handler stateMachineTick = new Handler() 
	{
		public void handleMessage(Message msg)
		{
			boolean retVal = true;
		
			if (soll != ist)
			{
				if (soll == noneMounted)
				{
					retVal = ist.unmountRequest();
				}
				else
				{
					retVal = ist.mountRequest();
				}
			}
			else
			{
				if (soll != noneMounted)
				{
					mainThreadRunHandler.sendEmptyMessage(1);
				}
			}
		}
	};
	

}
