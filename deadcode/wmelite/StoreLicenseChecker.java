package org.deadcode.wmelite;

import android.util.Log;

import com.google.android.vending.licensing.LicenseChecker;
import com.google.android.vending.licensing.LicenseCheckerCallback;
import com.google.android.vending.licensing.Policy;

public class StoreLicenseChecker {

    private static final String TAG = "LicCheck";

    private LicenseCheckerCallback krabatLicenseCheckerCallback;
    private LicenseChecker krabatLicenseChecker;

    private int    licenseCheckInProgress;
    private int    licenseCheckResult;
    private String licenseCheckDetail;

    public StoreLicenseChecker()
    {
        krabatLicenseCheckerCallback = new KrabatLicenseCheckerCallback();
    }

    public void init(LicenseChecker krabatLicenseChecker)
    {
        this.krabatLicenseChecker = krabatLicenseChecker;
        licenseCheckInProgress = 0;
        licenseCheckResult     = -1;
        licenseCheckDetail     = "n/a";
    }

    public void licenseCheckStart()
    {
        licenseCheckInProgress = 1;
        krabatLicenseChecker.checkAccess(krabatLicenseCheckerCallback);
    }

    public int licenseCheckPoll()
    {
        return licenseCheckInProgress;
    }

    public void licenseCheckAbort()
    {
        licenseCheckInProgress = 0;
    }

    public int licenseCheckResultStatus()
    {
        return licenseCheckResult;
    }

    public String licenseCheckResultDetail()
    {
        return licenseCheckDetail;
    }

    private class KrabatLicenseCheckerCallback implements LicenseCheckerCallback {
        public void allow(int policyReason) {
            // Should allow user access.

            Log.i(TAG, "License check success! PolicyReason=" + policyReason);

            licenseCheckDetail = "License ok : " + policyReason;

            licenseCheckInProgress = 0;
            licenseCheckResult     = 0;
        }

        public void dontAllow(int policyReason) {
            // Should not allow access. In most cases, the app should assume
            // the user has access unless it encounters this. If it does,
            // the app should inform the user of their unlicensed ways
            // and then either shut down the app or limit the user to a
            // restricted set of features.
            // In this example, we show a dialog that takes the user to Market.
            // If the reason for the lack of license is that the service is
            // unavailable or there is another problem, we display a
            // retry button on the dialog and a different message.
            Log.i(TAG, "License check problem! PolicyReason=" + policyReason);

            licenseCheckDetail = "License problem : " + policyReason;

            if (policyReason == Policy.RETRY)
            {
                // there was an issue during checking, maybe try again
                licenseCheckResult     = -1;
            }
            else
            {
                // check successful, result is unlicensed
                licenseCheckResult     = 1;
            }

            licenseCheckInProgress = 0;
        }

        public void applicationError(int errorCode) {
            // This is a polite way of saying the developer made a mistake
            // while setting up or calling the license checker library.
            // Please examine the error code and fix the error.

            Log.e(TAG, "License check error! Errorcode=" + errorCode);

            licenseCheckDetail = "Internal error : " + errorCode;

            licenseCheckInProgress = 0;
            licenseCheckResult     = -1;
        }
    }
}
