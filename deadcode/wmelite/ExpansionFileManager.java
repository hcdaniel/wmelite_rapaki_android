package org.deadcode.wmelite;

import org.deadcode.wmelite.expansion.ExpansionFileStateMachine;

import android.os.Handler;
import android.os.storage.StorageManager;
import android.util.Log;

public class ExpansionFileManager {

	private static ExpansionFileManager instance = null;
	
    private static final String TAG = "ExpansionMgr";

    private final ExpansionFileStateMachine stateMachine;
    
    private final String mainObbFile;
    
    private final String patchObbFile;

    public static ExpansionFileManager getInstance(String mainObbFile, String patchObbFile, StorageManager stMgr)
    {
    	if (instance == null)
    	{
    		instance = new ExpansionFileManager(mainObbFile, patchObbFile, stMgr);
    	}
    	
    	if ((instance.mainObbFile.equals(mainObbFile) == false) || (instance.patchObbFile.equals(patchObbFile) == false))
    	{
    		Log.e(TAG, "Trying to instanciate an ExpansionFileManager with different files! " 
    				+ instance.mainObbFile + " vs " + mainObbFile + ", " 
    				+ instance.patchObbFile + " vs " + patchObbFile);
    	}
    	
    	return instance;
    }
    
    /**
     * Assure only one instance per app, regardless of lifecycle.
     * 
     */
    private ExpansionFileManager(String mainObbFile, String patchObbFile, StorageManager stMgr)
    {
        this.mainObbFile = mainObbFile;
        this.patchObbFile = patchObbFile;
        
        stateMachine = new ExpansionFileStateMachine(stMgr, mainObbFile, patchObbFile);
    }

    public boolean needsMounting()
    {
        return true;
    }

    public String getObbMountPath()
    {
    	return "obbmount://" + stateMachine.getObbMountPath();
    }

    public String getObbMountPathPatch()
    {
    	return "obbmount://" + stateMachine.getObbMountPathPatch();
    }

    /**
     * Trigger a mount of the first OBB file.
     *
     * @param mainThreadRunHandler
     */
    public void startNewMount(Handler mainThreadRunHandler)
    {
        stateMachine.setCallbackHandler(mainThreadRunHandler);

        stateMachine.mountRequest();
    }

    /**
     * Trigger OBB unmounting.
     *
     */
    public void triggerUnmount()
    {
    	stateMachine.unmountRequest();
    }
}
