package org.deadcode.wmelite;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;

import com.android.vending.billing.IInAppBillingService;

public class InAppPurchaseServiceConnection implements ServiceConnection {

    private IInAppBillingService mIABService = null;

    private boolean isBound = false;

    @Override
    public void onServiceDisconnected(ComponentName name) {
        mIABService = null;
    }

    @Override
    public void onServiceConnected(ComponentName name,
                                   IBinder service) {
        mIABService = IInAppBillingService.Stub.asInterface(service);
    }

    public void setBound(boolean isBound) {
        this.isBound = isBound;
    }

    public boolean checkBound() {
        return isBound;
    }

    public IInAppBillingService getServiceObject()
    {
        return mIABService;
    }
}
