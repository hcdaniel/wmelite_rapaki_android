package org.deadcode.wmelite;

import java.nio.charset.Charset;
import java.nio.charset.IllegalCharsetNameException;
import java.nio.charset.UnsupportedCharsetException;

import java.util.ArrayList;

import com.android.vending.billing.IInAppBillingService;
import com.google.android.vending.licensing.LicenseChecker;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.content.res.Configuration;

import android.net.Uri;

import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Environment;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.storage.StorageManager;
import android.util.Log;

import org.json.JSONObject;
import org.json.JSONException;

import org.deadcode.wmelite.util.IabBroadcastReceiver.IabBroadcastListener;

public class WMELiteFunctions implements IabBroadcastListener {

	private static final String TAG = "WmeLiteFuncs";

	private Context c;

	private Handler iabStartPurchaseHandler;
	
	private ExpansionFileManager expFilesMgr;

	private StoreLicenseChecker stLicCkeck = new StoreLicenseChecker();

	public InAppPurchaseServiceConnection mIABServiceConn = new InAppPurchaseServiceConnection();

	public WMELiteFunctions() {
	}
	
	public boolean setContext(Context c, Handler mainThreadRunHandler, String mainObb, String patchObb, LicenseChecker krabatLicenseChecker, Handler iabStartPurchaseHandler) {
		this.c = c;
		this.iabStartPurchaseHandler = iabStartPurchaseHandler;

		stLicCkeck.init(krabatLicenseChecker);

		Log.i(TAG, "WMELiteFuncs: Forwarded obb files " + mainObb + ", " + patchObb);
		
		expFilesMgr = ExpansionFileManager.getInstance(mainObb, patchObb, (StorageManager) c.getSystemService(Context.STORAGE_SERVICE));
		
		// check whether we need the storage manager
		if (expFilesMgr.needsMounting() == true)
		{
			expFilesMgr.startNewMount(mainThreadRunHandler);

			return true;
		}
		else
		{
			return false;
		}
	}
	
	public void init() {
		nativeInit(c.getAssets());
	}
	
	public void exit() {
		// check whether we need the storage manager
		if (expFilesMgr.needsMounting() == true)
		{
			expFilesMgr.triggerUnmount();
		}
	}
	
	private native void nativeInit(AssetManager manager);
	
	public String getLogFileDirectory() {
		// this assumed the "external" storage exists and is mounted r/w
		return Environment.getExternalStorageDirectory().getAbsolutePath();
		
		// return "/mnt/sdcard/";
	}
	
	public String getPrivateFilesPath() {
		// this returns the app-private storage for user data
		return c.getFilesDir().getAbsolutePath();
	}

	public String getDeviceTypeHint() {
		if (c != null) {
			return ((c.getResources().getConfiguration().screenLayout
	            & Configuration.SCREENLAYOUT_SIZE_MASK)
	            >= Configuration.SCREENLAYOUT_SIZE_LARGE) ? "tablet" : "phone";
		} else {
			return "tablet";
		}
	}

	public String getGamePackagePath() {
		return expFilesMgr.getObbMountPath();
	}
	
	public String getGamePackagePatchPath() {
		return expFilesMgr.getObbMountPathPatch();
	}
	
	public String getGameFilePath() {
		// place the files here that regular WME does not package, i.e. fonts                        
		return "asset://raw";
	}

	public String getFontPath() {
		if (c != null) {
			// FIXME is this correct?
			return "asset://raw";
		} else {
			return ".";
		}
	}
	
	public String getLocalSettingsPath() {
		// for development and debugging, a local settings.xml
		// can be placed onto the device using a location
		// that can be accessed by both wmelite and the user
		
		return "asset://raw";
	}
	
	public byte[] getEncodedString(String inputString, String charsetName) {
		
		// change the default here if all the strings used in the project have the same encoding
		Charset charset = Charset.forName("US-ASCII");
		
		if (charsetName != null) {
			try {
				charset = Charset.forName(charsetName);
			} catch (IllegalCharsetNameException e1) {
				System.err.println("Charset name " + charsetName + " is illegal, using default!");
			} catch (UnsupportedCharsetException e2) {
				System.err.println("Charset name " + charsetName + " is not supported, using default!");
			}
		}
		
		// System.out.println("Encoding string '" + inputString + "' to charset " + charset.name());
		
		byte[] res = inputString.getBytes(charset);
		
		// System.out.println("Input len=" + inputString.length() + " to output byte array len=" + res.length);
		
		return res; 
	}
	
	public String getUTFString(byte[] inputBytes, String charsetName) {
		// change the default here if all the strings used in the project have the same encoding
		Charset charset = Charset.forName("US-ASCII");
		
		if (charsetName != null) {
			try {
				charset = Charset.forName(charsetName);
			} catch (IllegalCharsetNameException e1) {
				System.err.println("Charset name " + charsetName + " is illegal, using default!");
			} catch (UnsupportedCharsetException e2) {
				System.err.println("Charset name " + charsetName + " is not supported, using default!");
			}
		}
		
		String res = new String(inputBytes, charset);
		
		return res;
	}
	
	
	public void showURLInBrowser(String urlToShow)
	{
	  Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(urlToShow));
	  c.startActivity(myIntent);
	}

	public String getPlatformIdentifierString()
	{
	    String platformIdentifier = Build.MANUFACTURER + ":" + Build.PRODUCT + ":" + Build.MODEL + ":" + Build.VERSION.SDK_INT;
		return platformIdentifier.replaceAll("\\s","_");
	}

    public String getAppVersionString()
    {
        int appVersionCode;
        String appVersionName;

        String appPackageName = c.getPackageName();
        PackageInfo pi;
        try {
            pi = c.getPackageManager().getPackageInfo(appPackageName, PackageManager.GET_META_DATA);
            appVersionCode = pi.versionCode;
            appVersionName = pi.versionName;
        }
        catch (PackageManager.NameNotFoundException e){
            Log.e(TAG, "Error when querying package info:", e);

            appVersionCode = -1;
            appVersionName = "";
        }

        return appVersionName + ":" + appVersionCode;
    }

    public void licenseCheckStart()
	{
		stLicCkeck.licenseCheckStart();
	}
	
	public int licenseCheckPoll()
	{
		return stLicCkeck.licenseCheckPoll();
	}
	
	public void licenseCheckAbort()
	{
		stLicCkeck.licenseCheckAbort();
	}
	
	public int licenseCheckResultStatus()
	{
		return stLicCkeck.licenseCheckResultStatus();
	}
	
	public String licenseCheckResultDetail()
	{
		return stLicCkeck.licenseCheckResultDetail();
	}

    public void iapVerifyProductsList(String prodIdList)
	{
    	// split at ";"
		String[] prodIds = prodIdList.split(";");
		final ArrayList<String> skuList = new ArrayList<String> ();
		for (String prodId : prodIds)
		{
			if (prodId.length() > 1)
			{
				System.out.println("Query for product ID: " + prodId);
				skuList.add(prodId);
			}
		}
		
		// asynchronous query for all products
		final Bundle querySkus = new Bundle();
		querySkus.putStringArrayList("ITEM_ID_LIST", skuList);
		new Thread() {
			public void run() {
				
				// START callback deletes all previous valid/invalid products
				IAPReceiveProductsStartCallback();

				IInAppBillingService mIABService = mIABServiceConn.getServiceObject();

				if (mIABService != null)
				{
					try {
						Bundle skuDetails = mIABService.getSkuDetails(3,
							c.getPackageName(), "inapp", querySkus);
					
						int response = skuDetails.getInt("RESPONSE_CODE");
						if (response == 0) {
							ArrayList<String> responseList
								= skuDetails.getStringArrayList("DETAILS_LIST");
	
							for (String thisResponse : responseList) {
								JSONObject object = new JSONObject(thisResponse);
							
								if (object != null)
								{
									String sku = object.getString("productId");
									String price = object.getString("price");
									String title = object.getString("title");
									String description = object.getString("description");
								
									// add valid items to list
									IAPAddValidProductCallback(sku, title, description, price);
								
									// remove from the request list
									skuList.remove(sku);
								}
							}
						
							// all remaining items from the request are invalid (they do not exist)
							for (String invalidProductId : skuList)
							{
								IAPAddInvalidProductCallback(invalidProductId);
							}

						}
						else {
							System.out.println("Inapp query products status code=" + response);
						}
					
					} catch (RemoteException re) {
						re.printStackTrace();
					} catch (JSONException je) {
						je.printStackTrace();
					}
				}
				else {
					System.out.println("Inapp billing service is NULL");
				}

				
				// END callback generates "ProductsValidated" event in engine
				IAPReceiveProductsEndCallback();
			}
		}.start();
	}
	
	public void iapStartPurchase(String productId)
	{
		Message msg = iabStartPurchaseHandler.obtainMessage();
		msg.obj = productId;
		iabStartPurchaseHandler.sendMessage(msg);
	}

	/**
	 * Use a getter to not expose the service object to other classes.
	 *
	 * @return
	 */
	public Bundle iapGetBuyIntent(String packageName, String currPurchaseSku) throws RemoteException {
		Bundle b = null;
        IInAppBillingService mIABService = mIABServiceConn.getServiceObject();

		b = mIABService.getBuyIntent(3,
				packageName,
				currPurchaseSku,
				"inapp",
				"bGoa+V7g/yqDXvKRqq+JTFn4uQZbPiQJo4pf9RzJ");

		return b;
	}
	
	@Override
    public void receivedBroadcast() {
        System.out.println("Received PURCHASES_UPDATED broadcast notification. Querying inventory.");
        iapRefreshPreviousPurchases(false);
    }

	
	// used to either restore existing purchases, or to update status after a purchase
	public void iapRefreshPreviousPurchases(final boolean isRestore)
	{
		new Thread() {
			public void run() {
				
				// START callback removes previous transaction list
				IAPReceiveTransactionsStartCallback();
				
				int restoreError = 0;
                IInAppBillingService mIABService = mIABServiceConn.getServiceObject();

				if (mIABService != null)
				{

					try {
					
						Bundle ownedItems = mIABService.getPurchases(3, c.getPackageName(), "inapp", null);
					
						int response = ownedItems.getInt("RESPONSE_CODE");
					
						System.out.println("Inapp get purchases status code=" + response);
					
						if (response == 0) {
							ArrayList<String> ownedSkus =
								ownedItems.getStringArrayList("INAPP_PURCHASE_ITEM_LIST");
							ArrayList<String>  purchaseDataList =
								ownedItems.getStringArrayList("INAPP_PURCHASE_DATA_LIST");
							ArrayList<String>  signatureList =
								ownedItems.getStringArrayList("INAPP_DATA_SIGNATURE_LIST");
							String continuationToken =
								ownedItems.getString("INAPP_CONTINUATION_TOKEN");
	
							System.out.println("Inapp get purchases list size=" + purchaseDataList.size());
							
							for (int i = 0; i < purchaseDataList.size(); ++i) {
								// String signature = signatureList.get(i);
								String sku = ownedSkus.get(i);
								String purchaseData = purchaseDataList.get(i);
	
								JSONObject jo = new JSONObject(purchaseData);
								String jsonStatus = jo.getString("purchaseState");
								String state = "failed";
								if (jsonStatus.equals("0"))
								{
									state = "restored";
								}
								else if (jsonStatus.equals("1"))
								{
									state = "cancelled";
								}
							
								System.out.println("Inapp purchase " + i + " sku='" + sku + "' state=" + state);
							
								IAPAddTransactionCallback("trans_" + sku, sku, state);
							}
						}
					} catch (RemoteException re) {
						re.printStackTrace();
						restoreError = 1;
					} catch (JSONException je) {
						je.printStackTrace();
						restoreError = 1;
					}
				}
				
				// END callback generates "TransactionsUpdated" event in engine
				IAPReceiveTransactionsEndCallback();
				
				// generate restore successful/failed callback
				if (isRestore == true)
				{
					IAPRestoreFinishedCallback(restoreError);
				}
			}
		}.start();
	}
	
	public native void IAPAddValidProductCallback(String id, String name, String desc, String price);

	public native void IAPAddInvalidProductCallback(String id);

	public native void IAPReceiveProductsStartCallback();

	public native void IAPReceiveProductsEndCallback();

	public native void IAPAddTransactionCallback(String transId, String productId, String state);

	public native void IAPReceiveTransactionsStartCallback();

	public native void IAPReceiveTransactionsEndCallback();

	public native void IAPRestoreFinishedCallback(int error);

}
